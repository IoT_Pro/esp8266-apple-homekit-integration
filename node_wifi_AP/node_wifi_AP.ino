#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

ESP8266WebServer server(80);

IPAddress AP_IP(182, 130, 112, 123);

const char *ssid = "TP-LINK-ABC";
const char *password = "BBB";

void handleRoot() {
  
}

void handleNotFound() {
  
}

void setup() {
  Serial.begin(115200);
  Serial.println("\n");
  Serial.println("=============== Configure WiFi AP ===============");

  // setup access point
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAPConfig(AP_IP, AP_IP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssid, password);

  // print my IP
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
}

void loop() {
  server.handleClient();  
}


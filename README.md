# README
This is Apple HomeKit Integration Project.

## Install HomeBridge on Raspberry Pi
You should run following command in terminal.

    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install npm
    sudo npm cache clean -f
    sudo npm install -g n
    sudo n stable
    sudo apt-get install -y libavahi-compat-libdnssd-dev
    sudo npm install -g --unsafe-perm homebridge hap-nodejs node-gyp
    cd /usr/local/lib/node_modules/homebridge/
    sudo npm install --unsafe-perm bignum
    cd /usr/local/lib/node_modules/hap-nodejs/node_modules/mdns
    sudo node-gyp BUILDTYPE=Release rebuild
    sudo which node-gyp BUILDTYPE=Release rebuild
    
You can check whether homebridge is installed correctly with following command.

    which homebridge
    homebridge = $(which homebridge)
    $homebridge

If Ok, you should run homebridge core by following command.

    cd /usr/local/lib/node_modules/hap-nodejs/ 
    sudo node Core.js
    
Then, when you run HomeKit app in your iPhone, you can add accessories from Homebridge on Raspberry Pi.
At that time, you use pincode 03145154 instead of QR code.

## Install paho-mqtt, mosquitto on Raspberry Pi

    sudo apt-get install mosquitto -y
    sudo apt-get install mosquitto-clients -y
    sudo pip install paho-mqtt
    
## Add Accessory to HomeBridge
You should add accessory file (*.js) in /usr/local/lib/node_modules/hap-nodejs/accessories.
Accessory file sample is as follows.

    var Accessory = require('../').Accessory;
    var Service = require('../').Service;
    var Characteristic = require('../').Characteristic;
    var uuid = require('../').uuid;
    var PythonShell = require('python-shell');    
    
    var ACTest = exports.accessory = new Accessory('TestAccessory', uuid.generate('hap-nodejs:accessories:test'));
    
    // add properties for publishing (in case we are using Core.js and not BridgedCore.js)
    ACTest.username = "1A:2B:3C:4D:5E:FF";
    ACTest.pincode = "031-45-154";
    
    // set some basic properties (these values are arbitrary and setting them is optional)
    ACTest
        .getService(Service.AccessoryInformation)
        .setCharacteristic(Characteristic.Manufacturer, "Sample Company")
        
    var LightService = ACTest.addService(Service.Lightbulb, 'AC Light');

    LightService.getCharacteristic(Characteristic.On)
        .on('get', function(callback) {
          callback(null, ACTest_data.LightOn);
        })
        .on('set', function(value, callback) {
          ACTest_data.LightOn=value;
          console.log( "Characteristic Light On changed to %s",value);
          if (value) {
                PythonShell.run('accessories/light1.py', function (err) {
                    if (err) {
                        console.log('Err msg', err);
                    }
                    console.log('On Success');
                });
            } else {
                PythonShell.run('accessories/light0.py', function (err) {
                    console.log("Off Success");
                });
            }
          callback();
        });

## Issues
### file path problem in node js
You should point out file path as follows.

    PythonShell.run('accessories/light1.py', function (err) {
        ...
    }

Here, light1.py file is in 'accessories' folder.
That is, 'foldername/filename' is right.
'../filename', './filename', './foldername/filename', ... were not worked correctly.

### Include <ESP8266WiFi.h> problem in Arduino IDE
Some error occurs when include <ESP8266.h> in Arduino IDE normally.
To resolve this error, you should do like below.

    - Follow install instruction in https://github.com/esp8266/Arduino#using-git-version.
      Here, Use Installing with Boards Manager, Using git versions.
    - Select Board "NodeMCU 1.0 (ESP-12E Module)":
    - Select proper COM port.
    
### Include <PubNub.h> problem in Arduino IDE
Some error occurs when include <PubNub.h> in Arduino IDE.
To resolve this error, you should include following header at first.

    #include <EthernetClient.h>
    
### Swift Error 1
When press button, following error occur.
    
    unrecognized selector sent to instance 0x101d539f0
    2018-03-05 09:07:51.502033+0800 ESP8266LED[3344:2326927] *** Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '-[ESP8266LED.ViewController led1on:]: unrecognized selector sent to instance 0x101d539f0'

To resolve this, you should declare function as below:

    @IBAction func led1on(sender: AnyObject) {
        ...
    }
    
    Here, sender -> _ sender, That is,
    
    @IBAction func led1on(_ sender: AnyObject) {
        ...
    }
 
### Add Accessory Problem
You should Accessory file name like - "XXX_accessory.js"

Ex: NodeMCU_accessory.js

Contents of this file is like this:

    var Accessory = require('../').Accessory;
    var Service = require('../').Service;
    var Characteristic = require('../').Characteristic;
    var uuid = require('../').uuid;
    var err = null; // in case there were any problems
    var PythonShell = require('python-shell');
    
    // here's a fake hardware device that we'll expose to HomeKit
    var FAKE_OUTLET = {
        setPowerOn: function(on) {
        console.log("Turning the outlet %s!...", on ? "on" : "off");
        if (on) {
              FAKE_OUTLET.powerOn = true;
              if(err) { return console.log(err); }
              console.log("...outlet is now on.");
              PythonShell.run('accessories/NodeMCU_led_on.py', function (err) {
                    if (err) {
                        console.log('Err msg', err);
                    }
                    console.log('NodeMCU Led On Success');
                });
        } else {
              FAKE_OUTLET.powerOn = false;
              if(err) { return console.log(err); }
              console.log("...outlet is now off.");
              PythonShell.run('accessories/NodeMCU_led_off.py', function (err) {
                    if (err) {
                        console.log('Err msg', err);
                    }
                    console.log('NodeMCU Led Off Success');
                });
        }
      },
        identify: function() {
        console.log("Identify the outlet.");
        }
    }
    
    // Generate a consistent UUID for our outlet Accessory that will remain the same even when
    // restarting our server. We use the `uuid.generate` helper function to create a deterministic
    // UUID based on an arbitrary "namespace" and the accessory name.
    var outletUUID = uuid.generate('hap-nodejs:accessories:node');
    
    // This is the Accessory that we'll return to HAP-NodeJS that represents our fake light.
    var outlet = exports.accessory = new Accessory('ESP8266', outletUUID);
    
    // Add properties for publishing (in case we're using Core.js and not BridgedCore.js)
    outlet.username = "3A:2B:3C:4D:5D:2F";
    outlet.pincode = "031-45-154";
    
    // set some basic properties (these values are arbitrary and setting them is optional)
    outlet
      .getService(Service.AccessoryInformation)
      .setCharacteristic(Characteristic.Manufacturer, "Oltica")
      .setCharacteristic(Characteristic.Model, "Rev-1")
      .setCharacteristic(Characteristic.SerialNumber, "A1S2NASF88EW");
    
    // listen for the "identify" event for this Accessory
    outlet.on('identify', function(paired, callback) {
      FAKE_OUTLET.identify();
      callback(); // success
    });
    
    // Add the actual outlet Service and listen for change events from iOS.
    // We can see the complete list of Services and Characteristics in `lib/gen/HomeKitTypes.js`
    outlet
      .addService(Service.Outlet, "ESP8266") // services exposed to the user should have "names" like "Fake Light" for us
      .getCharacteristic(Characteristic.On)
      .on('set', function(value, callback) {
        FAKE_OUTLET.setPowerOn(value);
        callback(); // Our fake Outlet is synchronous - this value has been successfully set
      });
    
    // We want to intercept requests for our current power state so we can query the hardware itself instead of
    // allowing HAP-NodeJS to return the cached Characteristic.value.
    outlet
      .getService(Service.Outlet)
      .getCharacteristic(Characteristic.On)
      .on('get', function(callback) {

            // this event is emitted when you ask Siri directly whether your light is on or not. you might query
            // the light hardware itself to find this out, then call the callback. But if you take longer than a
            // few seconds to respond, Siri will give up.
        
            var err = null; // in case there were any problems
        
            if (FAKE_OUTLET.powerOn) {
              console.log("Are we on? Yes.");
              callback(err, true);
            }
            else {
              console.log("Are we on? No.");
              callback(err, false);
            }
    });  

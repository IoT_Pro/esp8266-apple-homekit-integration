#include <ESP8266WiFi.h>

WiFiServer server(80);

const char* ssid = "TP-LINK-REAL3D";
const char* password = "dkanehahffk";

void setup() {
  Serial.begin(115200);

  // prepare GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, 0);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.print("Server IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // Check if a client has connected
  WiFiClient wifi_client = server.available();
  if (!wifi_client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while(!wifi_client.available()){
    delay(1);
  }

  // Read the first line of the request
  String req = wifi_client.readStringUntil('\r');
  Serial.println(req);
  wifi_client.flush();

  // Match the request
  int val;
  if (req.indexOf("/gpio/0") != -1)
    val = 0;
  else if (req.indexOf("/gpio/1") != -1)
    val = 1;
  else {
    Serial.println("invalid request");
    wifi_client.stop();
    return;
  }

  // Set GPIO2 according to the request
  digitalWrite(2, val);
}


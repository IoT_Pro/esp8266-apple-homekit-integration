#include <ESP8266WiFi.h> 
#include <PubSubClient.h> 

const int ledPin = D3;

// WiFi
const char* ssid = "TP-LINK-REAL3D";
const char* wifi_password = "dkanehahffk";

// MQTT
const char* mqtt_server = "192.168.1.119";
const char* mqtt_topic = "test";
const char* mqtt_username = "user";
const char* mqtt_password = "1";
const char* clientID = "ESP8266_1";

WiFiClient wifiClient;
PubSubClient mqtt_client(mqtt_server, 1883, wifiClient);

void ReceivedMessage(char* topic, byte* payload, unsigned int length) {
  Serial.println((char*)payload);
  if ((char)payload[0] == '1') {
    digitalWrite(ledPin, HIGH); // Notice for the HUZZAH Pin 0, HIGH is OFF and LOW is ON. Normally it is the other way around.
    Serial.println("Turn On Pin 3");
  }
  if ((char)payload[0] == '0') {
    digitalWrite(ledPin, LOW);
    Serial.println("Turn Off Pin 3");
  }
}

bool Connect() {
  if (mqtt_client.connect(clientID, mqtt_username, mqtt_password)) {
      mqtt_client.subscribe(mqtt_topic);
      return true;
  } else {
      return false;
  }
}

void setup() {
  pinMode(ledPin, OUTPUT);
  
  Serial.begin(115200);  

  // Connect to the WiFi
  WiFi.begin(ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // Debugging - Output the IP Address of the ESP8266
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Connect to MQTT Broker
  mqtt_client.setCallback(ReceivedMessage);
  if (Connect()) {
    Serial.println("Connected Successfully to MQTT Broker!");  
  }
  else {
    Serial.println("Connection Failed!");
  }
}

void loop() {
  // If the connection is lost, try to connect again
  if (!mqtt_client.connected()) {
    Connect();
  }
  // client.loop() just tells the MQTT client code to do what it needs to do itself (i.e. check for messages, etc.)
  mqtt_client.loop();
}

